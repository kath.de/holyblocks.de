FROM ruby:2.7.0 AS builder

ENV LC_ALL C.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

WORKDIR /src
RUN gem install bundler

ADD Gemfile Gemfile.lock /src/
RUN bundle install

ADD . /src/
RUN bundle exec jekyll build

FROM nginx:1.17-alpine AS web

WORKDIR /app

COPY --from=builder /src/_build /usr/share/nginx/html
