---
title: Fortbildung
description: Der Verein publicatio e.V. bietet Fortbildungen für pastorale Mitarbeitende an, um religiöse Inhalte und Erfahrungen in dem bei Jugendlichen beliebten Computerspiel “Minecraft” zu vermitteln.
image:
  cover: https://www.publicatio-verein.de/s/cc_images/cache_14723325.jpg?t=1596216501
redirect_to: https://www.publicatio-verein.de/fortbildung/
---

<header class="hero">
  <div class="container">
    <div class="hero-body">
      <h1 class="title">{{ page.title}}</h1>
    </div>
  </div>
</header>
<section class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-8-desktop content">
        <img src="https://www.publicatio-verein.de/s/cc_images/cache_14723325.jpg?t=1596216501" />
        <p>
          Der Verein publicatio e.V. bietet Fortbildungen für pastorale Mitarbeitende an, um religiöse Inhalte und Erfahrungen in dem bei Jugendlichen beliebten Computerspiel “Minecraft” zu vermitteln.
        </p>
        <a href="https://www.publicatio-verein.de/fortbildung/" class="button">Zur Fortbildung bei publicatio e.V.</a>
      </div>
     </div>
  </div>
</section>
