BUILD_DIR := _build

JEKYLL := bundle exec jekyll
WEASYPRINT := weasyprint
SERVE := serve

$(BUILD_DIR): _build

.phony: build
build:
	$(JEKYLL) build

.phony: serve
serve:
	$(JEKYLL) serve

$(BUILD_DIR)/anleitungen/minecraft-spielen/index.html: _anleitungen/minecraft-spielen.html
	$(JEKYLL) build

assets/anleitungen:
	mkdir -p $@

assets/anleitungen/minecraft-spielen.pdf: assets/anleitungen $(BUILD_DIR)/anleitungen/minecraft-spielen/index.html
	$(SERVE) $(BUILD_DIR) -p 84234 & \
	serve_pid="$$!" && \
	$(WEASYPRINT) http://localhost:84234/anleitungen/minecraft-spielen/ $@ \
		-s _sass/print-short.css && \
	kill $$serve_pid
